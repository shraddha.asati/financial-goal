export const goalOptions = [
    { value: 'Housing', label: 'Housing' },
    { value: 'Income', label: 'Income' },
    { value: 'Investments', label: 'Investments' },
    { value: 'Savings', label: 'Savings' }
  ];
  
  export const goalTypes: { [key: string]: string[] } = {
    Housing: ['Avoid Eviction', 'Purchase a Home', 'Spends on Investments', 'Planning'],
    Income: ['Income Source', 'Increase Salary', 'Additional Income Source'],
    Investments: ['Spend on Investments', 'Retirement Fund', 'Provident Fund', 'Provisional Provident Fund'],
    Savings: ['Emergency Fund', 'Education Fund', 'Retirement Fund']
  };

  export const displayedColumns: string[] = ['Goal','Description','Target Date','Actions',];

  export const goals: {
    goalType: string;
    selectedGoal: string;
    subGoals: {
      description: string;
      targetDate: string;
      selectedGoal: string
    }[];
  }[] = [];
 
  
  
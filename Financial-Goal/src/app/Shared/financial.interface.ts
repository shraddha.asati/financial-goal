export interface Goal {
    goalType: string;
    selectedGoal: string;
    subGoals: SubGoal[];
  }
  
  export interface SubGoal {
    description: string;
    targetDate: string;
    selectedGoal: string;
  }
  
  
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FinancialService {

  constructor() { }

  public getValue(key: string) {
    let keyString: string | null = sessionStorage.getItem(key); 
    if (keyString === null) {
      return null; 
    }
    return JSON.parse(keyString); 
  }
  
  public storeValue(key:string, value: any) {
    let stringValue= JSON.stringify(value);
    sessionStorage.setItem(key, stringValue);
  }
 
  public clearData(){
  return sessionStorage.clear()
}
}

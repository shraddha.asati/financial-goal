import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FinancialGoalComponent } from './financial-goal/financial-goal.component';

const routes: Routes = [
  { path: '' , component:FinancialGoalComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

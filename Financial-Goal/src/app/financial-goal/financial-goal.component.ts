import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Goal, SubGoal } from '../Shared/financial.interface';
import { goalOptions, goalTypes, displayedColumns, goals} from '../Shared/financial.constant';
import {FinancialService} from '../financial.service';

@Component({
  selector: 'app-financial-goal',
  templateUrl: './financial-goal.component.html',
  styleUrls: ['./financial-goal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class FinancialGoalComponent implements OnInit {
  
  public selectedGoalType: string = '';
  public selectedGoalValue: string = '';
  public selectedSubGoalValue: string = '';
  public goalOptions = goalOptions;
  public goalTypes = goalTypes;
  public selectedOptions: string[] = [];
  public selectedOptionsSubGoal: string[] = [];
  public goals = goals;
  public displayedColumns = displayedColumns;
  public dataSource: SubGoal[] = [];

  constructor(private financialservice:FinancialService) {
     // Retrieving stored data from FinancialService
    let  storedGoalSData= this.financialservice.getValue('storedValue')
    let  storedGoalOptionSData= this.financialservice.getValue('storedValueOption')
    let  storedGoalSubOptionData= this.financialservice.getValue('storedValueSubOption')

    if(storedGoalSData && storedGoalSData.length>0)
    {
       // Parsing stored data and assigning values
    this.goals=storedGoalSData
   this.selectedGoalValue=this.goals[0].goalType
    this.selectedOptions=storedGoalOptionSData
    this.selectedOptionsSubGoal=storedGoalSubOptionData
    }

  }

  ngOnInit(): void {}
  

  /**
   * Adds a new financial goal.
   */
  public addGoal() {
    if (this.selectedGoalType) {
      this.selectedGoalValue = this.selectedGoalType;
      this.goals.push({
        goalType: this.selectedGoalValue,
        selectedGoal: '',
        subGoals: [],
      });
      this.selectedOptions.push(this.selectedGoalType);
      this.selectedGoalType = '';
    }
  }

  /**
   * Checks if an option is disabled.
   * @param option The option to check.
   * @returns A boolean indicating if the option is disabled.
   */
  public isOptionDisabled(option: string): boolean {
    return this.selectedOptions.includes(option);
  }

  /**
   * Adds a sub-goal under a financial goal.
   * @param goal The financial goal to which the sub-goal will be added.
   */
  public addSubGoal(goal: Goal) {
    if (goal.selectedGoal) {
      this.selectedSubGoalValue = goal.selectedGoal;
      goal.subGoals.push({
        description: '',
        targetDate: '',
        selectedGoal: this.selectedSubGoalValue,
      });
      goal.subGoals = [...goal.subGoals];
      this.selectedOptionsSubGoal.push(goal.selectedGoal);
      goal.selectedGoal = '';
    }
  }

  /**
   * Checks if a sub-goal option is disabled.
   * @param option The option to check.
   * @returns A boolean indicating if the sub-goal option is disabled.
   */
  isOptionDisabledsubGoal(option: string): boolean {
    return this.selectedOptionsSubGoal.includes(option);
  }

  /**
   * Removes a financial goal.
   * @param goal The financial goal to remove.
   */
  public removeGoal(goal: Goal) {
    const index = this.goals.indexOf(goal);
    if (index !== -1) {
      this.goals.splice(index, 1);
    }
    const indexx = this.selectedOptions.indexOf(goal.goalType);
    if (indexx !== -1) {
      this.selectedOptions.splice(indexx, 1);
    }
  }

  /**
   * Removes a sub-goal under a financial goal.
   * @param goal The financial goal from which the sub-goal will be removed.
   * @param index The index of the sub-goal to remove.
   * @param seletedGoal The sub-goal to remove.
   */
  public removeSubGoal(goal: Goal, index: number, seletedGoal: SubGoal) {
    goal.subGoals.splice(index, 1);
    goal.subGoals = [...goal.subGoals];
    const indexx = this.selectedOptionsSubGoal.indexOf(
      seletedGoal.selectedGoal
    );
    if (indexx !== -1) {
      this.selectedOptionsSubGoal.splice(indexx, 1);
    }
  }

  /**
   * Checks if the form is valid.
   * @returns A boolean indicating if the form is valid.
   */
  public isValid() {
    return (
      this.goals.length > 0 &&
      this.goals.every(
        (goal) =>
          goal.subGoals.length > 0 &&
          goal.subGoals.every(
            (subGoal: any) =>
              subGoal.description.trim() !== '' &&
              subGoal.targetDate
          )
      )
    );
  }

  /**
   * Save the data
   */
  public save() {
     this.financialservice.storeValue('storedValue',this.goals)
     this.financialservice.storeValue('storedValueOption',this.selectedOptions)
     this.financialservice.storeValue('storedValueSubOption',this.selectedOptionsSubGoal)
  }

  /**
   * Clear the data from the table
   */
  public clearForm(){
    this.financialservice.clearData()
    this.selectedGoalValue="";
    this.selectedOptions=[];
    this.selectedOptionsSubGoal=[];
    this.goals=[];
  }

  
 
  
}

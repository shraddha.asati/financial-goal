import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { FinancialGoalComponent } from './financial-goal.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';



describe('FinancialGoalComponent', () => {
  let component: FinancialGoalComponent;
  let fixture: ComponentFixture<FinancialGoalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FinancialGoalComponent],
      providers: [FormBuilder],
      imports: [
        BrowserAnimationsModule,
        MatFormFieldModule,
        MatSelectModule,
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FinancialGoalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add a new financial goal', () => {
    component.selectedGoalType = 'Housing';
    component.addGoal();
    expect(component.goals.length).toEqual(1);
  });

  it('should check if an option is disabled', () => {
    component.selectedOptions = ['Housing', 'Income'];
    expect(component.isOptionDisabled('Housing')).toBeTrue();
    expect(component.isOptionDisabled('Investments')).toBeFalse();
  });

  it('should remove a financial goal', () => {
    const goal = { goalType: 'Housing', selectedGoal: '', subGoals: [] };
    component.goals.push(goal);
    component.selectedOptions.push('Housing');
    component.removeGoal(goal);
    expect(component.goals.length).toEqual(0);
    expect(component.selectedOptions.length).toEqual(0);
  });


  it('should toggle content visibility', () => {
    component.showMore = true;
    component.toggleContent();
    expect(component.showMore).toBeFalse();
    component.toggleContent();
    expect(component.showMore).toBeTrue();
  });
});
